CREATE USER db_user WITH PASSWORD 'abc123';
GRANT SELECT, UPDATE, DELETE ON 
	usuario_financiero, 
	persona_natural, 
	persona_juridica, 
	ciudad_persona_natural, 
	ciudad_persona_juridica 
TO db_user;