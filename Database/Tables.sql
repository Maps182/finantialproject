CREATE DATABASE financiero;

--IMPORTANTE: Acceder a la base de datos mediante el Query Tool

-- -----------------------------------------------------
-- Table "Usuario Financiero"
-- -----------------------------------------------------
CREATE TABLE  usuario_financiero (
  "id" SERIAL NOT NULL UNIQUE,
  "fecha_creacion" TIMESTAMP NOT NULL,
  "usuario_creacion" VARCHAR(20) NOT NULL UNIQUE,
  "fecha_modificacion" TIMESTAMP NOT NULL,
  "usuario_modificacion" VARCHAR(20) NOT NULL UNIQUE,
  PRIMARY KEY ("id"));

COMMENT ON COLUMN usuario_financiero.id is 'ID usuario financiero';
COMMENT ON COLUMN usuario_financiero.fecha_creacion is 'Fecha de creacion por parte de un usuario';
COMMENT ON COLUMN usuario_financiero.usuario_creacion is 'Usario creacion';
COMMENT ON COLUMN usuario_financiero.fecha_modificacion is 'Fecha de modificacion por parte de un usuario';
COMMENT ON COLUMN usuario_financiero.usuario_modificacion is 'Usuario modificacion';

-- -----------------------------------------------------
-- Table "Ciudad Persona Natural"
-- -----------------------------------------------------
CREATE TABLE ciudad_persona_natural (
  "id" SERIAL NOT NULL UNIQUE,
  "ciudad_vivienda" VARCHAR(15) NOT NULL,
  "direccion_domicilio" VARCHAR(80) NOT NULL,
  PRIMARY KEY ("id"));

COMMENT ON COLUMN ciudad_persona_natural.id is 'ID ciudad de la persona natural';
COMMENT ON COLUMN ciudad_persona_natural.ciudad_vivienda is 'Ciudad donde vive la persona';
COMMENT ON COLUMN ciudad_persona_natural.direccion_domicilio is 'Direccion de domicilio de la persona';

-- -----------------------------------------------------
-- Table "Persona_natural"
-- -----------------------------------------------------
CREATE TABLE persona_natural (
  "id" SERIAL NOT NULL UNIQUE,
  "id_usuario_financiero" INT NOT NULL,
  "id_ciudad_persona_natural" INT NOT NULL,
  "primer_nombre" VARCHAR(20) NOT NULL,
  "segundo_nombre" VARCHAR(20) NULL,
  "primer_apellido" VARCHAR(20) NOT NULL,
  "segundo_apellido" VARCHAR(20) NOT NULL,
  "nro_documento_identidad" NUMERIC NOT NULL,
  "celular" NUMERIC NULL,
  PRIMARY KEY ("id"),
  CONSTRAINT "fk_PERSONA_NATURAL_CIUDAD"
    FOREIGN KEY ("id_ciudad_persona_natural")
    REFERENCES ciudad_persona_natural("id")
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT "fk_PERSONA_NATURAL_FINANCIERO"
    FOREIGN KEY ("id_usuario_financiero")
    REFERENCES usuario_financiero("id")
    ON DELETE NO ACTION
	ON UPDATE NO ACTION
);

COMMENT ON COLUMN persona_natural.id is 'ID persona natural';
COMMENT ON COLUMN persona_natural.id_usuario_financiero is 'Llave foranea de usuario financiero';
COMMENT ON COLUMN persona_natural.id_ciudad_persona_natural is 'Llave foranea de la persona natural';
COMMENT ON COLUMN persona_natural.primer_nombre is 'Primer nombre de la persona';
COMMENT ON COLUMN persona_natural.segundo_nombre is 'Segundo nombre de la persona si es que tiene';
COMMENT ON COLUMN persona_natural.primer_apellido is 'Primer apellido de la persona';
COMMENT ON COLUMN persona_natural.segundo_apellido is 'Segundo apellido de la persona';
COMMENT ON COLUMN persona_natural.nro_documento_identidad is 'Numero de documento de identidad de la persona';
COMMENT ON COLUMN persona_natural.celular is 'Celular de la persona';

-- -----------------------------------------------------
-- Table "Ciudad Persona Juridica"
-- -----------------------------------------------------
CREATE TABLE ciudad_persona_juridica (
  "id" SERIAL NOT NULL UNIQUE,
  "ciudad_empresa" VARCHAR(15) NOT NULL,
  "direccion" VARCHAR(15) NOT NULL,
  PRIMARY KEY ("id"));

COMMENT ON COLUMN ciudad_persona_juridica.id is 'ID de la ciudad de la persona juridica';
COMMENT ON COLUMN ciudad_persona_juridica.ciudad_empresa is 'Ciudad donde se ubica la empresa';
COMMENT ON COLUMN ciudad_persona_juridica.direccion is 'Direccion de la empresa';

-- -----------------------------------------------------
-- Table "Persona Juridica"
-- -----------------------------------------------------
CREATE TABLE persona_juridica (
  "id" SERIAL NOT NULL UNIQUE,
  "id_usuario_financiero" INT NOT NULL,
  "id_ciudad_persona_juridica" INT NOT NULL,
  "razon_social" VARCHAR(40) NOT NULL,
  "nit" NUMERIC NOT NULL,
  "telefono" NUMERIC NOT NULL,
  PRIMARY KEY ("id"),
  CONSTRAINT "fk_PERSONA_JURIDICA_FINANCIERO"
    FOREIGN KEY ("id_usuario_financiero")
    REFERENCES usuario_financiero("id")
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT "fk_PERSONA_JURIDICA_CIUDAD"
    FOREIGN KEY ("id_ciudad_persona_juridica")
    REFERENCES ciudad_persona_juridica("id")
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

COMMENT ON COLUMN persona_juridica.id is 'ID de la persona juridica';
COMMENT ON COLUMN persona_juridica.id_usuario_financiero is 'Llave foranea del usuario financiero';
COMMENT ON COLUMN persona_juridica.id_ciudad_persona_juridica is 'Llave foranea de la persona juridica';
COMMENT ON COLUMN persona_juridica.razon_social is 'Razon social de la empresa';
COMMENT ON COLUMN persona_juridica.nit is 'Nit de la empresa';