# FinantialProject



## Getting started

This project is meant to create a person with its respective finantial user.

## Backend
### Execution
In the database we have basically two users, one with the whole privileges and the other created just to read,
update and delete.

**User with Privileges**
For this user it is neccesary to have a local user with the whole privileges.

This user is for **dev** environment.

![finantial1](https://github.com/RodrigoValda/TestTitanWordpress/assets/86843637/e5ed7ef5-6238-484c-98ff-30c8a3812772)

**Brand New user**
This user was created for this project.

This user is for **pdn** environment.

![finantial2](https://github.com/RodrigoValda/TestTitanWordpress/assets/86843637/72dfd0d0-a37c-411f-9622-7f6acd521e48)


**Change environent**

You can change environment among **pdn** and **dev**.

![finantial3](https://github.com/RodrigoValda/TestTitanWordpress/assets/86843637/885d35db-ec34-41f0-83a8-e853c2c1d06c)


## Frontend
### Execution
Execute the the following command line:

```bash
ng serve --open
```

## License
All rights reserved.
