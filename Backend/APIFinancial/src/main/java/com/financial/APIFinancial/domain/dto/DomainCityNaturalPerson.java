package com.financial.APIFinancial.domain.dto;

public class DomainCityNaturalPerson {
    private Integer idCityNaturalPerson;
    private String housingCity;
    private String homeAddress;

    public Integer getIdCityNaturalPerson() {
        return idCityNaturalPerson;
    }

    public void setIdCityNaturalPerson(Integer idCityNaturalPerson) {
        this.idCityNaturalPerson = idCityNaturalPerson;
    }

    public String getHousingCity() {
        return housingCity;
    }

    public void setHousingCity(String housingCity) {
        this.housingCity = housingCity;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }
}
