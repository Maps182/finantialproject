package com.financial.APIFinancial.persistence.crud;

import com.financial.APIFinancial.persistence.entities.CityNaturalPerson;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CityNaturalPersonaCrudRepository extends JpaRepository<CityNaturalPerson, Integer> {
    Page<CityNaturalPerson> findAll(Pageable pageable);
}
