package com.financial.APIFinancial.persistence.mapper;

import com.financial.APIFinancial.domain.dto.CreationDomainNaturalPerson;
import com.financial.APIFinancial.domain.dto.DomainNaturalPerson;
import com.financial.APIFinancial.persistence.entities.NaturalPerson;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring", uses = {CityNaturalPersonMapper.class, FinancialUserMapper.class})
public interface CreationNaturalPersonMapper {
    @Mappings({
            @Mapping(source = "idPersonaNatural", target = "idNaturalPerson"),
            @Mapping(source = "primerNombre", target = "firstName"),
            @Mapping(source = "segundoNombre", target = "middleName"),
            @Mapping(source = "primerApellido", target = "firstLastName"),
            @Mapping(source = "segundoApellido", target = "secondLastName"),
            @Mapping(source = "documentoIndentidad", target = "identification"),
            @Mapping(source = "celular", target = "cellphone"),
            @Mapping(source = "idUsuarioFinanciero", target = "idFinancialUser"),
            @Mapping(source = "idCiudadPersonaNatural", target = "idCityNaturalPerson")

    })
    CreationDomainNaturalPerson toDomainCreationNaturalPerson(NaturalPerson naturalPerson);

    List<CreationDomainNaturalPerson> toDomainCreationNaturalPeople(List<NaturalPerson> naturalPeopleList);

    @InheritInverseConfiguration
    @Mapping(target = "financialUser", ignore = true)
    @Mapping(target = "cityNaturalPerson", ignore = true)
    NaturalPerson toCreationNaturalPerson(CreationDomainNaturalPerson creationDomainNaturalPerson);
}
