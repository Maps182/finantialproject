package com.financial.APIFinancial.domain.service;

import com.financial.APIFinancial.domain.dto.CreationDomainNaturalPerson;
import com.financial.APIFinancial.domain.dto.DomainFinancialUser;
import com.financial.APIFinancial.domain.dto.DomainNaturalPerson;
import com.financial.APIFinancial.domain.repository.NaturalPersonDTO;
import com.financial.APIFinancial.persistence.entities.NaturalPerson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class NaturalPersonService {
    @Autowired
    private NaturalPersonDTO naturalPersonDTO;

    public List<DomainNaturalPerson> getAll(Pageable pageable) {
        return naturalPersonDTO.getAll(pageable);
    }

    public Optional<List<DomainNaturalPerson>> getFinancialUserByFirstName(String firstName) {
        return naturalPersonDTO.getNaturalPersonByFirstName(firstName);
    }

    public Optional<List<DomainNaturalPerson>> getFinancialUserByLastName(String lastName) {
        return naturalPersonDTO.getNaturalPersonByFirstLastname(lastName);
    }

    public Optional<List<DomainNaturalPerson>> getNaturalPersonByFirstNameAndLastName(String firstName, String lastName) {
        return naturalPersonDTO.getNaturalPersonByFirstNameAndFirstLastname(firstName, lastName);
    }
    public Optional<DomainNaturalPerson> getNaturalPerson(int idNaturalPerson) {
        return naturalPersonDTO.getNaturalPerson(idNaturalPerson);
    }

    public CreationDomainNaturalPerson save(CreationDomainNaturalPerson creationDomainNaturalPerson) {
        return naturalPersonDTO.save(creationDomainNaturalPerson);
    }

    public boolean delete(int idNaturalPerson) {
        return getNaturalPerson(idNaturalPerson).map(person -> {
            naturalPersonDTO.delete(idNaturalPerson);
            return true;
        }).orElse(false);
    }
}
