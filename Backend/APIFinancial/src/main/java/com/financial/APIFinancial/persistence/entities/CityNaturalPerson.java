package com.financial.APIFinancial.persistence.entities;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Null;
import jakarta.validation.constraints.Size;

@Entity
@Table(name = "ciudad_persona_natural")
public class CityNaturalPerson {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idCiudadPersonaNatural;

    @Column(name = "ciudad_vivienda")
    @NotNull(message = "Value must not be null")
    @Size(max = 15, message = "Max quantity of characters must be 15")
    private String ciudadVivienda;

    @Column(name = "direccion_domicilio")
    @NotNull(message = "Value must not be null")
    @Size(max = 80, message = "Max quantity of characters must be 80")
    private String direccionDomicilio;

    @OneToOne(mappedBy = "cityNaturalPerson", cascade = {CascadeType.ALL})
    private NaturalPerson naturalPersonCity;

    public Integer getIdCiudadPersonaNatural() {
        return idCiudadPersonaNatural;
    }

    public void setIdCiudadPersonaNatural(Integer idCiudadPersonaNatural) {
        this.idCiudadPersonaNatural = idCiudadPersonaNatural;
    }

    public String getCiudadVivienda() {
        return ciudadVivienda;
    }

    public void setCiudadVivienda(String ciudadVivienda) {
        this.ciudadVivienda = ciudadVivienda;
    }

    public String getDireccionDomicilio() {
        return direccionDomicilio;
    }

    public void setDireccionDomicilio(String direccionDomicilio) {
        this.direccionDomicilio = direccionDomicilio;
    }

    public NaturalPerson getNaturalPersonCity() {
        return naturalPersonCity;
    }
    public void setNaturalPersonCity(NaturalPerson naturalPersonCity) {
        this.naturalPersonCity = naturalPersonCity;
    }
}
