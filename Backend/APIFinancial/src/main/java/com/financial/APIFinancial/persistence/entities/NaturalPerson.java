package com.financial.APIFinancial.persistence.entities;

import jakarta.persistence.*;
import jakarta.validation.constraints.DecimalMax;
import jakarta.validation.constraints.Size;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Digits;

@Entity
@Table(name = "persona_natural")
public class NaturalPerson {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idPersonaNatural;

    @Column(name = "id_usuario_financiero")
    @NotNull(message = "Value must not be null")
    private Integer idUsuarioFinanciero;

    @Column(name = "id_ciudad_persona_natural")
    @NotNull(message = "Value must not be null")
    private Integer idCiudadPersonaNatural;

    @Column(name = "primer_nombre")
    @NotNull(message = "Value must not be null")
    @Size(max = 20, message = "Max quantity of characters must be 20")
    private String primerNombre;

    @Column(name = "segundo_nombre")
    @Size(max = 20, message = "Max quantity of characters must be 20")
    private String segundoNombre;

    @Column(name = "primer_apellido")
    @NotNull(message = "Value must not be null")
    @Size(max = 20, message = "Max quantity of characters must be 20")
    private String primerApellido;

    @Column(name = "segundo_apellido")
    @NotNull(message = "Value must not be null")
    @Size(max = 20, message = "Max quantity of characters must be 20")
    private String segundoApellido;

    @Column(name = "nro_documento_identidad")
    @NotNull(message = "Value must not be null")
    @DecimalMax(value = "99999999", inclusive = true, message = "The max number is 99999999")
    @Digits(integer = 8, fraction = 0, message = "the value only must contain eight digits and must be an integer")
    private Integer documentoIndentidad;

    @Column(name = "celular")
    @NotNull(message = "Value must not be null")
    @Digits(integer = 8, fraction = 0, message = "the value only must contain eight digits and must be an integer")
    private Integer celular;

    @OneToOne(cascade = {CascadeType.ALL})
    @MapsId("idUsuarioFinanciero")
    @JoinColumn(name = "id_usuario_financiero", updatable = false, insertable = false)
    private FinancialUser financialUser;

    @OneToOne(cascade = {CascadeType.ALL})
    @MapsId("idCiudadPersonaNatural")
    @JoinColumn(name = "id_ciudad_persona_natural", updatable = false, insertable = false)
    private CityNaturalPerson cityNaturalPerson;

    public Integer getIdPersonaNatural() {
        return idPersonaNatural;
    }

    public void setIdPersonaNatural(Integer idPersonaNatural) {
        this.idPersonaNatural = idPersonaNatural;
    }

    public Integer getIdUsuarioFinanciero() {
        return idUsuarioFinanciero;
    }

    public void setIdUsuarioFinanciero(Integer idUsuarioFinanciero) {
        this.idUsuarioFinanciero = idUsuarioFinanciero;
    }

    public Integer getIdCiudadPersonaNatural() {
        return idCiudadPersonaNatural;
    }

    public void setIdCiudadPersonaNatural(Integer idCiudadPersonaNatural) {
        this.idCiudadPersonaNatural = idCiudadPersonaNatural;
    }

    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getSegundoNombre() {
        return segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public Integer getDocumentoIndentidad() {
        return documentoIndentidad;
    }

    public void setDocumentoIndentidad(Integer documentoIndentidad) {
        this.documentoIndentidad = documentoIndentidad;
    }

    public Integer getCelular() {
        return celular;
    }

    public void setCelular(Integer celular) {
        this.celular = celular;
    }

    public FinancialUser getFinancialUser() {
        return financialUser;
    }

    public void setFinancialUser(FinancialUser financialUser) {
        this.financialUser = financialUser;
    }

    public CityNaturalPerson getCityNaturalPerson() {
        return cityNaturalPerson;
    }

    public void setCityNaturalPerson(CityNaturalPerson cityNaturalPerson) {
        this.cityNaturalPerson = cityNaturalPerson;
    }
}
