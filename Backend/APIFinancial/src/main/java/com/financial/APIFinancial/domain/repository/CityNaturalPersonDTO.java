package com.financial.APIFinancial.domain.repository;

import com.financial.APIFinancial.domain.dto.DomainCityNaturalPerson;
import com.financial.APIFinancial.persistence.entities.CityNaturalPerson;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface CityNaturalPersonDTO {
    List<DomainCityNaturalPerson> getAll(Pageable pageable);
    Optional<DomainCityNaturalPerson> getCityNaturalPerson(int idCityNaturalPerson);
    DomainCityNaturalPerson save(DomainCityNaturalPerson domainCityNaturalPerson);
    void delete(int idCityNaturalPerson);
}
