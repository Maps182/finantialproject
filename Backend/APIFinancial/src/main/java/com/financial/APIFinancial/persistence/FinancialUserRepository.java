package com.financial.APIFinancial.persistence;

import com.financial.APIFinancial.domain.dto.DomainFinancialUser;
import com.financial.APIFinancial.domain.repository.FinancialUserDTO;
import com.financial.APIFinancial.persistence.crud.FinancialUserCrudRepository;
import com.financial.APIFinancial.persistence.entities.CityNaturalPerson;
import com.financial.APIFinancial.persistence.entities.FinancialUser;
import com.financial.APIFinancial.persistence.mapper.FinancialUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class FinancialUserRepository implements FinancialUserDTO {
    @Autowired
    FinancialUserCrudRepository financialUserCrudRepository;

    @Autowired
    FinancialUserMapper mapper;

    @Override
    public List<DomainFinancialUser> getAll(Pageable pageable) {
        List<FinancialUser> financialUserList = new ArrayList<>();
        Page<FinancialUser> financialUserPage = financialUserCrudRepository.findAll(pageable);
        financialUserPage.forEach(financialUserList::add);
        return mapper.toDomainFinancialPeople(financialUserList);
    }

    @Override
    public Optional<DomainFinancialUser> getFinancialUser(int idFinancialuser) {
        return financialUserCrudRepository.findById(idFinancialuser).map(
                user -> mapper.toDomainFinancialPerson(user)
        );
    }

    @Override
    public DomainFinancialUser save(DomainFinancialUser domainFinancialUser) {
        FinancialUser financialUser = mapper.toFinancialUser(domainFinancialUser);
        return mapper.toDomainFinancialPerson(financialUserCrudRepository.save(financialUser));
    }

    @Override
    public void delete(int idFinancialuser) {
        financialUserCrudRepository.deleteById(idFinancialuser);
    }
}
