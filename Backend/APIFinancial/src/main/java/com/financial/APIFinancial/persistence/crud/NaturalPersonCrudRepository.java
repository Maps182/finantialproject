package com.financial.APIFinancial.persistence.crud;

import com.financial.APIFinancial.persistence.entities.FinancialUser;
import com.financial.APIFinancial.persistence.entities.NaturalPerson;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;;import java.util.List;
import java.util.Optional;

public interface NaturalPersonCrudRepository extends JpaRepository<NaturalPerson, Integer> {
    Page<NaturalPerson> findAll(Pageable pageable);

    @Query("SELECT np FROM FinancialUser fu " +
            "INNER JOIN NaturalPerson np " +
            "ON fu.idUsuarioFinanciero = np.idUsuarioFinanciero " +
            "WHERE np.primerNombre = :primerNombre")
    Optional<List<NaturalPerson>> findByPrimerNombreIdNaturalPerson(
            @Param("primerNombre") String primerNombre
    );

    @Query("SELECT np FROM FinancialUser fu " +
            "INNER JOIN NaturalPerson np " +
            "ON fu.idUsuarioFinanciero = np.idUsuarioFinanciero " +
            "WHERE np.primerApellido = :primerApellido")
    Optional<List<NaturalPerson>> findByPrimerApellidoIdNaturalPerson(
            @Param("primerApellido") String primerApellido
    );

    @Query("SELECT np FROM FinancialUser fu " +
            "INNER JOIN NaturalPerson np " +
            "ON fu.idUsuarioFinanciero = np.idUsuarioFinanciero " +
            "WHERE np.primerNombre = :primerNombre " +
            "AND np.primerApellido = :primerApellido " +
            "ORDER BY np.primerApellido ASC")
    Optional<List<NaturalPerson>> findByPrimerNombreIdNaturalPersonAndPrimerApellidoIdNaturalPersonOrderByPrimerNombreIdNaturalPerson(
            @Param("primerNombre") String primerNombre,
            @Param("primerApellido") String primerApellido
    );
}
