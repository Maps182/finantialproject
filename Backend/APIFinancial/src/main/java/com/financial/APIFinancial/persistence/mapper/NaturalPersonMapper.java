package com.financial.APIFinancial.persistence.mapper;

import com.financial.APIFinancial.domain.dto.DomainNaturalPerson;
import com.financial.APIFinancial.persistence.entities.NaturalPerson;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring", uses = {CityNaturalPersonMapper.class, FinancialUserMapper.class})
public interface NaturalPersonMapper {
    @Mappings({
            @Mapping(source = "idPersonaNatural", target = "idNaturalPerson"),
            @Mapping(source = "primerNombre", target = "firstName"),
            @Mapping(source = "segundoNombre", target = "middleName"),
            @Mapping(source = "primerApellido", target = "firstLastName"),
            @Mapping(source = "segundoApellido", target = "secondLastName"),
            @Mapping(source = "documentoIndentidad", target = "identification"),
            @Mapping(source = "celular", target = "cellphone"),
            @Mapping(source = "idUsuarioFinanciero", target = "idFinancialUser"),
            @Mapping(source = "idCiudadPersonaNatural", target = "idCityNaturalPerson"),
            @Mapping(source = "financialUser", target = "domainFinancialUser"),
            @Mapping(source = "cityNaturalPerson", target = "domainCityNaturalPerson")

    })
    DomainNaturalPerson toDomainNaturalPerson(NaturalPerson naturalPerson);

    List<DomainNaturalPerson> toDomainNaturalPeople(List<NaturalPerson> naturalPeopleList);

    @InheritInverseConfiguration
    NaturalPerson toNaturalPerson(DomainNaturalPerson domainNaturalPerson);
}
