package com.financial.APIFinancial.persistence;

import com.financial.APIFinancial.domain.dto.CreationDomainNaturalPerson;
import com.financial.APIFinancial.domain.dto.DomainFinancialUser;
import com.financial.APIFinancial.domain.dto.DomainNaturalPerson;
import com.financial.APIFinancial.domain.repository.NaturalPersonDTO;
import com.financial.APIFinancial.persistence.crud.NaturalPersonCrudRepository;
import com.financial.APIFinancial.persistence.entities.FinancialUser;
import com.financial.APIFinancial.persistence.entities.NaturalPerson;
import com.financial.APIFinancial.persistence.mapper.CreationNaturalPersonMapper;
import com.financial.APIFinancial.persistence.mapper.NaturalPersonMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class NaturalPersonRepository implements NaturalPersonDTO {
    @Autowired
    NaturalPersonCrudRepository naturalPersonCrudRepository;

    @Autowired
    NaturalPersonMapper mapper;

    @Autowired
    CreationNaturalPersonMapper createMapper;

    @Override
    public List<DomainNaturalPerson> getAll(Pageable pageable) {
        List<NaturalPerson> naturalPersonList = new ArrayList<>();
        Page<NaturalPerson> naturalPersoPage = naturalPersonCrudRepository.findAll(pageable);
        naturalPersoPage.forEach(naturalPersonList::add);
        return mapper.toDomainNaturalPeople(naturalPersonList);
    }

    @Override
    public Optional<DomainNaturalPerson> getNaturalPerson(int idNaturalPerson) {
        return naturalPersonCrudRepository.findById(idNaturalPerson).map(
                user -> mapper.toDomainNaturalPerson(user)
        );
    }

    @Override
    public Optional<List<DomainNaturalPerson>> getNaturalPersonByFirstName(String firstName) {
        Optional<List<NaturalPerson>> financialUsers = naturalPersonCrudRepository.findByPrimerNombreIdNaturalPerson(firstName);
        return financialUsers.map(person -> mapper.toDomainNaturalPeople(person));
    }

    @Override
    public Optional<List<DomainNaturalPerson>> getNaturalPersonByFirstLastname(String lastname) {
        Optional<List<NaturalPerson>> financialUsers = naturalPersonCrudRepository.findByPrimerApellidoIdNaturalPerson(lastname);
        return financialUsers.map(person -> mapper.toDomainNaturalPeople(person));
    }

    @Override
    public Optional<List<DomainNaturalPerson>> getNaturalPersonByFirstNameAndFirstLastname(String firstname, String lastname) {
        Optional<List<NaturalPerson>> financialUsers = naturalPersonCrudRepository
                .findByPrimerNombreIdNaturalPersonAndPrimerApellidoIdNaturalPersonOrderByPrimerNombreIdNaturalPerson(firstname, lastname);
        return financialUsers.map(person -> mapper.toDomainNaturalPeople(person));
    }

    @Override
    public CreationDomainNaturalPerson save(CreationDomainNaturalPerson creationDomainNaturalPerson) {
        NaturalPerson naturalPerson = createMapper.toCreationNaturalPerson(creationDomainNaturalPerson);
        return createMapper.toDomainCreationNaturalPerson(naturalPersonCrudRepository.save(naturalPerson));
    }

    @Override
    public void delete(int idNaturalPerson) {
        naturalPersonCrudRepository.deleteById(idNaturalPerson);
    }
}
