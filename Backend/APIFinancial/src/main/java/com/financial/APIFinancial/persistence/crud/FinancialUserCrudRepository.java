package com.financial.APIFinancial.persistence.crud;

import com.financial.APIFinancial.persistence.entities.FinancialUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface FinancialUserCrudRepository extends JpaRepository<FinancialUser, Integer> {
    Page<FinancialUser> findAll(Pageable pageable);
}
