package com.financial.APIFinancial.web.controller;

import com.financial.APIFinancial.domain.dto.DomainFinancialUser;
import com.financial.APIFinancial.domain.service.CityNaturalPersonService;
import com.financial.APIFinancial.domain.service.FinancialUserService;
import com.financial.APIFinancial.domain.service.NaturalPersonService;
import com.financial.APIFinancial.persistence.entities.FinancialUser;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "Financial User Section")
@RestController
@RequestMapping("/financial-users")
@CrossOrigin
public class FinancialUserController {
    @Autowired
    private FinancialUserService financialUserService;

    @GetMapping()
    @Operation(summary = "Get all the financial users filtering by page")
    @ApiResponse(responseCode = "200", description = "OK")
    public ResponseEntity<List<DomainFinancialUser>> getAll(
            @Parameter(description = "The page number", required = false, example = "1")
            @RequestParam(defaultValue = "0") int page,
            @Parameter(description = "The quantity of registers", required = false, example = "9")
            @RequestParam(defaultValue = "10") int size
    ) {
        Pageable pageable = PageRequest.of(page, size);
        return new ResponseEntity<>(financialUserService.getAllFinancialUsers(pageable), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @Operation(summary = "Search a financial user by its ID")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "404", description = "User not found")
    })
    public ResponseEntity<DomainFinancialUser> getAFinancialUser(
            @Parameter(description = "The ID of Financial User", required = true, example = "2")
            @PathVariable("id") int idFinancialUser
    ) {
        return financialUserService.getAFinancialUser(idFinancialUser)
                .map(user -> new ResponseEntity<>(user, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping("/save")
    @Operation(summary = "Create a financial user")
    @ApiResponse(responseCode = "201", description = "Created")
    public ResponseEntity<DomainFinancialUser> saveAFinancialUser(
            @RequestBody DomainFinancialUser domainFinancialUser
    ){
        return new ResponseEntity<>(financialUserService.save(domainFinancialUser), HttpStatus.CREATED);
    }

    @DeleteMapping("/delete/{id}")
    @Operation(summary = "Delete a financial user by its ID")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "404", description = "Financial user not found")
    })
    public ResponseEntity deleteAFinancialUser(
            @Parameter(description = "The ID of the financial user", required = true, example = "2")
            @PathVariable("id") int idFinancialUser
    ) {
        if (financialUserService.delete(idFinancialUser)) {
            return new ResponseEntity(HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
}
