package com.financial.APIFinancial.domain.repository;

import com.financial.APIFinancial.domain.dto.DomainFinancialUser;
import com.financial.APIFinancial.persistence.entities.FinancialUser;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface FinancialUserDTO {
    List<DomainFinancialUser> getAll(Pageable pageable);
    Optional<DomainFinancialUser> getFinancialUser(int idFinancialuser);
    DomainFinancialUser save(DomainFinancialUser domainFinancialUser);
    void delete(int idFinancialuser);
}
