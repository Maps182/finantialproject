package com.financial.APIFinancial.web.controller;

import com.financial.APIFinancial.domain.dto.CreationDomainNaturalPerson;
import com.financial.APIFinancial.domain.dto.DomainNaturalPerson;
import com.financial.APIFinancial.domain.service.NaturalPersonService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "Natural Person Section")
@RestController
@RequestMapping("/natural-person")
@CrossOrigin
public class NaturalPersonController {
    @Autowired
    private NaturalPersonService naturalPersonService;

    @GetMapping()
    @Operation(summary = "Get all the natural people filtering by page")
    @ApiResponse(responseCode = "200", description = "OK")
    public ResponseEntity<List<DomainNaturalPerson>> getAllNaturalPeople(
            @Parameter(description = "The page number", required = false, example = "1")
            @RequestParam(defaultValue = "0") int page,
            @Parameter(description = "The quantity of registers", required = false, example = "9")
            @RequestParam(defaultValue = "10") int size
    ) {
        Pageable pageable = PageRequest.of(page, size);
        return new ResponseEntity<>(naturalPersonService.getAll(pageable), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @Operation(summary = "Search a natural person by its ID")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "404", description = "Person not found")
    })
    public ResponseEntity<DomainNaturalPerson> getANaturalPerson(
            @Parameter(description = "The ID of Natural Person", required = true, example = "2")
            @PathVariable("id") int idNaturalPerson
    ) {
        return naturalPersonService.getNaturalPerson(idNaturalPerson)
                .map(city -> new ResponseEntity<>(city, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PutMapping("update/{id}")
    @Operation(summary = "Update a natural person by its ID")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "404", description = "Person not found")
    })
    public ResponseEntity<CreationDomainNaturalPerson> updateANaturalPerson(
            @Parameter(description = "The ID of Natural Person", required = true, example = "2")
            @PathVariable("id") int idNaturalPerson,
            @RequestBody CreationDomainNaturalPerson creationDomainNaturalPerson
    ) {
        return naturalPersonService.getNaturalPerson(idNaturalPerson)
                .map(city -> new ResponseEntity<>(naturalPersonService.save(creationDomainNaturalPerson), HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping("/first-name")
    @Operation(summary = "Search a natural person by its first name")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "404", description = "Person not found")
    })
    public ResponseEntity<List<DomainNaturalPerson>> getANaturalPersonByItsFirstName(
            @Parameter(description = "The first name of user", required = true, example = "Mauricio")
            @RequestParam String firstName
    ) {
        List<DomainNaturalPerson> domainFinancialUserList = naturalPersonService
                .getFinancialUserByFirstName(firstName)
                .orElse(null);

        return domainFinancialUserList != null && !domainFinancialUserList.isEmpty() ?
                new ResponseEntity<>(domainFinancialUserList, HttpStatus.OK)
                : new ResponseEntity<List<DomainNaturalPerson>>(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/last-name")
    @Operation(summary = "Search a natural person by its last name")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "404", description = "User not found")
    })
    public ResponseEntity<List<DomainNaturalPerson>> getANaturalPersonByItsLastName(
            @Parameter(description = "The last name of user", required = true, example = "Aliendre")
            @RequestParam String lastName
    ) {
        List<DomainNaturalPerson> domainFinancialUserList = naturalPersonService
                .getFinancialUserByLastName(lastName)
                .orElse(null);

        return domainFinancialUserList != null && !domainFinancialUserList.isEmpty() ?
                new ResponseEntity<>(domainFinancialUserList, HttpStatus.OK)
                : new ResponseEntity<List<DomainNaturalPerson>>(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/complete-name")
    @Operation(summary = "Search a natural person by its complete name")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "404", description = "User not found")
    })
    public ResponseEntity<List<DomainNaturalPerson>> getANaturalPersonByItsFirstNameAndLastName(
            @Parameter(description = "The first name of user", required = true, example = "Mauricio")
            @RequestParam String firstName,
            @Parameter(description = "The last name of user", required = true, example = "Aliendre")
            @RequestParam String lastName
    ) {
        List<DomainNaturalPerson> domainFinancialUserList = naturalPersonService
                .getNaturalPersonByFirstNameAndLastName(firstName, lastName)
                .orElse(null);

        return domainFinancialUserList != null && !domainFinancialUserList.isEmpty() ?
                new ResponseEntity<>(domainFinancialUserList, HttpStatus.OK)
                : new ResponseEntity<List<DomainNaturalPerson>>(HttpStatus.NOT_FOUND);
    }

    @PostMapping("/save")
    @Operation(summary = "Create a natural person")
    @ApiResponse(responseCode = "201", description = "Created")
    public ResponseEntity<CreationDomainNaturalPerson> saveANaturalPerson(
            @RequestBody CreationDomainNaturalPerson creationDomainNaturalPerson
    ){
        return new ResponseEntity<>(naturalPersonService.save(creationDomainNaturalPerson), HttpStatus.CREATED);
    }

    @DeleteMapping("/delete/{id}")
    @Operation(summary = "Delete a natural person by its ID")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "404", description = "City not found")
    })
    public ResponseEntity deleteANaturalPerson(
            @Parameter(description = "The ID of the natural person", required = true, example = "2")
            @PathVariable("id") int idCity
    ) {
        if (naturalPersonService.delete(idCity)) {
            return new ResponseEntity(HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
}
