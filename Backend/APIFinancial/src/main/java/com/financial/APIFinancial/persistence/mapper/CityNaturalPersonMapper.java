package com.financial.APIFinancial.persistence.mapper;

import com.financial.APIFinancial.domain.dto.DomainCityNaturalPerson;
import com.financial.APIFinancial.persistence.entities.CityNaturalPerson;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CityNaturalPersonMapper {
    @Mappings({
            @Mapping(source = "idCiudadPersonaNatural", target = "idCityNaturalPerson"),
            @Mapping(source = "ciudadVivienda", target = "housingCity"),
            @Mapping(source = "direccionDomicilio", target = "homeAddress")
    })
    DomainCityNaturalPerson toDomainCityNaturalPerson(CityNaturalPerson cityNaturalPerson);

    List<DomainCityNaturalPerson> toDomainCitiesNaturalPeople(List<CityNaturalPerson> cityNaturalPersonList);

    @InheritInverseConfiguration
    @Mapping(target = "naturalPersonCity", ignore = true)
    CityNaturalPerson toCityNaturalPerson(DomainCityNaturalPerson domainCityNaturalPerson);
}
