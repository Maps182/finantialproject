package com.financial.APIFinancial.domain.dto;

public class CreationDomainNaturalPerson {
    private Integer idNaturalPerson;
    private String firstName;
    private String middleName;
    private String firstLastName;
    private String secondLastName;
    private Integer identification;
    private Integer cellphone;

    private Integer idCityNaturalPerson;

    private Integer idFinancialUser;

    public Integer getIdNaturalPerson() {
        return idNaturalPerson;
    }

    public void setIdNaturalPerson(Integer idNaturalPerson) {
        this.idNaturalPerson = idNaturalPerson;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getFirstLastName() {
        return firstLastName;
    }

    public void setFirstLastName(String firstLastName) {
        this.firstLastName = firstLastName;
    }

    public String getSecondLastName() {
        return secondLastName;
    }

    public void setSecondLastName(String secondLastName) {
        this.secondLastName = secondLastName;
    }

    public Integer getIdentification() {
        return identification;
    }

    public void setIdentification(Integer identification) {
        this.identification = identification;
    }

    public Integer getCellphone() {
        return cellphone;
    }

    public void setCellphone(Integer cellphone) {
        this.cellphone = cellphone;
    }

    public Integer getIdCityNaturalPerson() {
        return idCityNaturalPerson;
    }

    public void setIdCityNaturalPerson(Integer idCityNaturalPerson) {
        this.idCityNaturalPerson = idCityNaturalPerson;
    }

    public Integer getIdFinancialUser() {
        return idFinancialUser;
    }

    public void setIdFinancialUser(Integer idFinancialUser) {
        this.idFinancialUser = idFinancialUser;
    }
}
