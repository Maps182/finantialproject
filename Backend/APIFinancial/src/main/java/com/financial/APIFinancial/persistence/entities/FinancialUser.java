package com.financial.APIFinancial.persistence.entities;

import jakarta.persistence.*;
import jakarta.validation.constraints.Size;
import jakarta.validation.constraints.NotNull;

import java.time.LocalDateTime;

@Entity
@Table(name = "usuario_financiero")
public class FinancialUser {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idUsuarioFinanciero;

    @Column(name = "fecha_creacion")
    @NotNull(message = "Date must not be null")
    private LocalDateTime fechaCreacion;

    @Column(name = "usuario_creacion")
    @NotNull(message = "User must not be null")
    @Size(max = 20, message = "Max quantity of characters must be 20")
    private String usuarioCreacion;

    @Column(name = "fecha_modificacion")
    @NotNull(message = "Date must not be null")
    private LocalDateTime fechaModificacion;

    @Column(name = "usuario_modificacion")
    @NotNull(message = "User must not be null")
    @Size(max = 20, message = "Max quantity of characters must be 20")
    private String usuarioModificacion;

    @OneToOne(mappedBy = "financialUser", cascade = {CascadeType.ALL})
    private NaturalPerson naturalPersonUser;

    public Integer getIdUsuarioFinanciero() {
        return idUsuarioFinanciero;
    }

    public void setIdUsuarioFinanciero(Integer idUsuarioFinanciero) {
        this.idUsuarioFinanciero = idUsuarioFinanciero;
    }

    public LocalDateTime getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(LocalDateTime fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public LocalDateTime getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(LocalDateTime fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public NaturalPerson getNaturalPersonUser() {
        return naturalPersonUser;
    }

    public void setNaturalPersonUser(NaturalPerson naturalPersonUser) {
        this.naturalPersonUser = naturalPersonUser;
    }
}
