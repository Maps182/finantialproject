package com.financial.APIFinancial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiFinancialApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiFinancialApplication.class, args);
	}

}
