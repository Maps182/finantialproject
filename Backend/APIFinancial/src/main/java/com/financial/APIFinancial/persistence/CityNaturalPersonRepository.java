package com.financial.APIFinancial.persistence;

import com.financial.APIFinancial.domain.dto.DomainCityNaturalPerson;
import com.financial.APIFinancial.domain.repository.CityNaturalPersonDTO;
import com.financial.APIFinancial.persistence.crud.CityNaturalPersonaCrudRepository;
import com.financial.APIFinancial.persistence.entities.CityNaturalPerson;
import com.financial.APIFinancial.persistence.mapper.CityNaturalPersonMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class CityNaturalPersonRepository implements CityNaturalPersonDTO {
    @Autowired
    CityNaturalPersonaCrudRepository cityNaturalPersonaCrudRepository;

    @Autowired
    CityNaturalPersonMapper mapper;

    @Override
    public List<DomainCityNaturalPerson> getAll(Pageable pageable) {
        List<CityNaturalPerson> cityNaturalPersonList = new ArrayList<>();
        Page<CityNaturalPerson> cityNaturalPersonPage = cityNaturalPersonaCrudRepository.findAll(pageable);
        cityNaturalPersonPage.forEach(cityNaturalPersonList::add);
        return mapper.toDomainCitiesNaturalPeople(cityNaturalPersonList);
    }

    @Override
    public Optional<DomainCityNaturalPerson> getCityNaturalPerson(int idCityNaturalPerson) {
        return cityNaturalPersonaCrudRepository.findById(idCityNaturalPerson).map(
                user -> mapper.toDomainCityNaturalPerson(user)
        );
    }

    @Override
    public DomainCityNaturalPerson save(DomainCityNaturalPerson domainCityNaturalPerson) {
        CityNaturalPerson cityNaturalPerson = mapper.toCityNaturalPerson(domainCityNaturalPerson);
        return mapper.toDomainCityNaturalPerson(cityNaturalPersonaCrudRepository.save(cityNaturalPerson));
    }

    @Override
    public void delete(int idCityNaturalPerson) {
        cityNaturalPersonaCrudRepository.deleteById(idCityNaturalPerson);
    }
}
