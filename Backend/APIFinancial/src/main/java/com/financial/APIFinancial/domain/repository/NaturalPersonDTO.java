package com.financial.APIFinancial.domain.repository;

import com.financial.APIFinancial.domain.dto.CreationDomainNaturalPerson;
import com.financial.APIFinancial.domain.dto.DomainFinancialUser;
import com.financial.APIFinancial.domain.dto.DomainNaturalPerson;
import com.financial.APIFinancial.persistence.entities.NaturalPerson;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface NaturalPersonDTO {
    List<DomainNaturalPerson> getAll(Pageable pageable);
    Optional<List<DomainNaturalPerson>> getNaturalPersonByFirstName(String firstName);
    Optional<List<DomainNaturalPerson>> getNaturalPersonByFirstLastname(String lastname);
    Optional<List<DomainNaturalPerson>> getNaturalPersonByFirstNameAndFirstLastname(String firstname, String lastname);
    Optional<DomainNaturalPerson> getNaturalPerson(int idNaturalPerson);
    CreationDomainNaturalPerson save(CreationDomainNaturalPerson creationDomainNaturalPerson);
    void delete(int idNaturalPerson);
}
