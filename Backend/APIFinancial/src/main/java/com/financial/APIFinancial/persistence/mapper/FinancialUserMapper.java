package com.financial.APIFinancial.persistence.mapper;

import com.financial.APIFinancial.domain.dto.DomainFinancialUser;
import com.financial.APIFinancial.persistence.entities.FinancialUser;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface FinancialUserMapper {
    @Mappings({
            @Mapping(source = "idUsuarioFinanciero", target = "idFinancialUser"),
            @Mapping(source = "fechaCreacion", target = "creationDate"),
            @Mapping(source = "usuarioCreacion", target = "creationUser"),
            @Mapping(source = "fechaModificacion", target = "modificationDate"),
            @Mapping(source = "usuarioModificacion", target = "modificationUser"),
    })
    DomainFinancialUser toDomainFinancialPerson(FinancialUser financialUser);

    List<DomainFinancialUser> toDomainFinancialPeople(List<FinancialUser> financialUserList);

    @InheritInverseConfiguration
    @Mapping(target = "naturalPersonUser", ignore = true)
    FinancialUser toFinancialUser(DomainFinancialUser domainFinancialUser);
}
