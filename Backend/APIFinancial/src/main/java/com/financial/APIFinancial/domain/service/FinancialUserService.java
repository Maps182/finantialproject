package com.financial.APIFinancial.domain.service;

import com.financial.APIFinancial.domain.dto.DomainFinancialUser;
import com.financial.APIFinancial.domain.repository.FinancialUserDTO;
import com.financial.APIFinancial.persistence.entities.FinancialUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FinancialUserService {
    @Autowired
    private FinancialUserDTO financialUserDTO;

    public List<DomainFinancialUser> getAllFinancialUsers(Pageable pageable) {
        return financialUserDTO.getAll(pageable);
    }

    public Optional<DomainFinancialUser> getAFinancialUser(int idFinancialUser) {
        return financialUserDTO.getFinancialUser(idFinancialUser);
    }

    public DomainFinancialUser save(DomainFinancialUser domainFinancialUser) {
        return financialUserDTO.save(domainFinancialUser);
    }

    public boolean delete(int idFinancialUser) {
        return getAFinancialUser(idFinancialUser).map(product -> {
            financialUserDTO.delete(idFinancialUser);
            return true;
        }).orElse(false);
    }
}
