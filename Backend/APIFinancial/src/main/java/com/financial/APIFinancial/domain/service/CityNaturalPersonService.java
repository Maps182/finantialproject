package com.financial.APIFinancial.domain.service;

import com.financial.APIFinancial.domain.dto.DomainCityNaturalPerson;
import com.financial.APIFinancial.domain.repository.CityNaturalPersonDTO;
import com.financial.APIFinancial.persistence.entities.CityNaturalPerson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CityNaturalPersonService {
    @Autowired
    private CityNaturalPersonDTO cityNaturalPersonDTO;

    public List<DomainCityNaturalPerson> getAll(Pageable pageable) {
        return cityNaturalPersonDTO.getAll(pageable);
    }

    public Optional<DomainCityNaturalPerson> getCity(int idCityNaturalPerson) {
        return cityNaturalPersonDTO.getCityNaturalPerson(idCityNaturalPerson);
    }

    public DomainCityNaturalPerson save(DomainCityNaturalPerson domainCityNaturalPerson) {
        return cityNaturalPersonDTO.save(domainCityNaturalPerson);
    }

    public boolean delete(int idCityNaturalPerson) {
        return getCity(idCityNaturalPerson).map(city -> {
            cityNaturalPersonDTO.delete(idCityNaturalPerson);
            return true;
        }).orElse(false);
    }
}
