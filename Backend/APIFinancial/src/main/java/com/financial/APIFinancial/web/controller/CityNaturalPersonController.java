package com.financial.APIFinancial.web.controller;

import com.financial.APIFinancial.domain.dto.DomainCityNaturalPerson;
import com.financial.APIFinancial.domain.service.CityNaturalPersonService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "City Natural Person Section")
@RestController
@RequestMapping("/city-natural-persons")
@CrossOrigin
public class CityNaturalPersonController {

    @Autowired
    private CityNaturalPersonService cityNaturalPersonService;

    @GetMapping()
    @Operation(summary = "Get all the cities filtering by page")
    @ApiResponse(responseCode = "200", description = "OK")
    public ResponseEntity<List<DomainCityNaturalPerson>> getAllCities(
            @Parameter(description = "The page number", required = false, example = "1")
            @RequestParam(defaultValue = "0") int page,
            @Parameter(description = "The quantity of registers", required = false, example = "9")
            @RequestParam(defaultValue = "10") int size
    ) {
        Pageable pageable = PageRequest.of(page, size);
        return new ResponseEntity<>(cityNaturalPersonService.getAll(pageable), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @Operation(summary = "Search a city of a natural person by its ID")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "404", description = "City not found")
    })
    public ResponseEntity<DomainCityNaturalPerson> getACityNaturalPersonUser(
            @Parameter(description = "The ID of City", required = true, example = "2")
            @PathVariable("id") int idCity
    ) {
        return cityNaturalPersonService.getCity(idCity)
                .map(city -> new ResponseEntity<>(city, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping("/save")
    @Operation(summary = "Create a city of a natural person")
    @ApiResponse(responseCode = "201", description = "Created")
    public ResponseEntity<DomainCityNaturalPerson> saveACity(
            @RequestBody DomainCityNaturalPerson domainCityNaturalPerson
    ){
        return new ResponseEntity<>(cityNaturalPersonService.save(domainCityNaturalPerson), HttpStatus.CREATED);
    }

    @DeleteMapping("/delete/{id}")
    @Operation(summary = "Delete a city of a natural person by its ID")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "404", description = "City not found")
    })
    public ResponseEntity deleteACity(
            @Parameter(description = "The ID of the city", required = true, example = "2")
            @PathVariable("id") int idCity
    ) {
        if (cityNaturalPersonService.delete(idCity)) {
            return new ResponseEntity(HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
}
